const prompt = require("prompt");

const InputManager = require("./InputManager");
const SecretNumberManager = require("./SecretNumberManager");

class Game {
  static GAME_STATUES = {
    IS_WINNER: 0,
    TIE: 0,
    IS_NOT_OVER: -1,
    BULL: 0,
    COW: 0,
    TRYS: 11,
  };

  static ASK_FOR_GUESS_PROPS = [
    {
      name: "guess",
      warning: "Guess must be only numbers, and at least 4 numbers long",
      conform: InputManager.guessValidator,
    },
  ];

  constructor() {
    this._inputManager = new InputManager();
    this._secretNumberManager = new SecretNumberManager();
    this.gameStatus = Game.GAME_STATUES.IS_NOT_OVER;
  }

  async askForGuess() {
    Game.GAME_STATUES.TRYS--;
    return prompt.get(Game.ASK_FOR_GUESS_PROPS);
  }

  async getGuessValidator(message) {
    console.log("\n" + message);
    let input = null;
    let guessNumber = "";
    do {
      try {
        const { guess } = await this.askForGuess();
        input = new InputManager(guess);
        guessNumber = guess;
      } catch (err) {
        console.log(err.message);
      }
    } while (!input);
    console.log(input);
    return input, guessNumber;
  }

  async checkGuess(guess) {
    const { isCow, isBull } = this._secretNumberManager.matchGuess(guess);

    console.log(isCow, isBull);

    Game.GAME_STATUES.COW = isCow;
    Game.GAME_STATUES.BULL = isBull;
  }

  async iterateGame() {
    while (this.gameStatus === Game.GAME_STATUES.IS_NOT_OVER) {
      // const player = this._players[this._playerToPlayNextIdx];
      console.log(
        `\n* ${Game.GAME_STATUES.BULL} ${
          Game.GAME_STATUES.BULL > 1 ? "bulls" : "bull"
        }`
      );
      console.log(
        `* ${Game.GAME_STATUES.COW} ${
          Game.GAME_STATUES.COW > 1 ? "cows" : "cow"
        }`
      );
      const { input } = await this.getGuessValidator();
      console.log(this._inputManager._guess);
      this.checkGuess(this._inputManager._guess);
      this.updateGameStatus();
    }
  }

  updateGameStatus = () => {
    if (Game.GAME_STATUES.BULL === 4) {
      Game.GAME_STATUES.IS_WINNER = 1;
      return (this.gameStatus = Game.GAME_STATUES.IS_WINNER);
    }

    if (Game.GAME_STATUES.TRYS === 0)
      return (this.gameStatus = Game.GAME_STATUES.TIE);
    return (this.gameStatus = Game.GAME_STATUES.IS_NOT_OVER);
  };

  printResults = () => {
    switch (this.gameStatus) {
      case Game.GAME_STATUES.IS_WINNER === 1:
        return console.log(
          `TOU WON! The secret number is "${this._secretNumberManager._secretNumber}"`
        );
      // case Game.GAME_STATUES.IS_WINNER===0:
      // return console.log(`YOU LOST! The secret number is "${this._secretNumberManager._secret}"!`);
      case Game.GAME_STATUES.TIE:
        return console.log(
          `YOU LOST! The secret number is "${this._secretNumberManager._secret}"!`
        );
      default:
        return console.log("Something went wrong");
    }
  };

  async start() {
    // await this.askForGuess();
    await this.iterateGame();
    this.printResults();
  }
}

module.exports = Game;
