class InputManager {
  constructor(guess) {
    this._guess = InputManager.guessValidator(guess);
  }

  static isValid = (value) => /^[0-9]{4}$/.test(value);

  static guessValidator = (guess) => {
    let guessValue = [];
    for (var i = 0; i < 4; i++) {
      if (InputManager.isValid(guess)) {
        guessValue = [...guessValue, guess.charAt(i)];

        guessValue = [...new Set(guessValue)];
      }
    }
    // console.log(guessValue);
    if (guessValue.length === 4) return guessValue;
  };

  get guess() {
    return this._guess;
  }

  set guess(newGuess) {
    this.guess = newGuess;
  }
}

module.exports = InputManager;
