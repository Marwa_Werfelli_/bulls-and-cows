class SecretNumberManager {
  constructor() {
    this._secretNumber = SecretNumberManager.giveRandomSecretNumber();
    console.log(this._secretNumber);
  }

  static getRandomNumber = () => {
    const value = Math.floor(Math.random() * 10);
    return value;
  };
  static checkIsValid = (number, otherNumber) =>
    /^[0-9]{4}$/.test(number) && number != otherNumber;

  static giveRandomSecretNumber = () => {
    let firstNumber = 0;
    let secondNumber = 0;
    let thirdNumber = 0;
    let forthNumber = 0;
    let secretNumber = [];
    do {
      firstNumber = SecretNumberManager.getRandomNumber();
      secondNumber = SecretNumberManager.getRandomNumber();
      thirdNumber = SecretNumberManager.getRandomNumber();
      forthNumber = SecretNumberManager.getRandomNumber();

      secretNumber = [
        ...secretNumber,
        firstNumber,
        secondNumber,
        thirdNumber,
        forthNumber,
      ];

      secretNumber = [...new Set(secretNumber)];
    } while (secretNumber.length < 4);

    const randomNumber =
      firstNumber.toString() +
      secondNumber.toString() +
      thirdNumber.toString() +
      forthNumber.toString();

    return randomNumber;
  };
  matchGuess = (guess) => {
    let isCow = 0;
    let isBull = 0;
    const count = Array(10).fill(0);
    console.log("gs:", guess);

    for (let i = 0; i < 4; i++) {
      if (this._secretNumber.toString()[i] === guess[i]) {
        isBull += 1;
      } else {
        if (count[this._secretNumber.toString()[i] - "0"] < 0) isCow += 1;
        if (count[guess[i] - "0"] > 0) isCow += 1;

        count[this._secretNumber.toString()[i] - "0"]++;
        count[guess[i] - "0"]--;
      }

      console.log(this._secretNumber.toString()[i]);
    }

    // let numberS = this._secretNumber.toString();
    // for (var i = 0; i < 4; i++) {
    //   if (numberS.indexOf(guess.toString()[i]) !== -1) {
    //     if (guess.toString()[i] === numberS[i]) {
    //       isBull++;
    //     } else {
    //       isCow++;
    //     }
    //   }
    //   console.log("g", guess.toString()[i]);
    // }

    console.log(isCow, isBull, numberS);
    return isCow, isBull;
  };
}

module.exports = SecretNumberManager;
